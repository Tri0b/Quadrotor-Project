#include "quadcopter.h"

//******************************** MPU6050 ********************************
#include "I2Cdev.h"

#include "MPU6050_6Axis_MotionApps20.h"

#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

#define INTERRUPT_PIN 2

MPU6050 mpu;
// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float euler[3];         // [psi, theta, phi]    Euler angle container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

// packet structure for InvenSense teapot demo
uint8_t teapotPacket[14] = { '$', 0x02, 0,0, 0,0, 0,0, 0,0, 0x00, 0x00, '\r', '\n' };

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
    mpuInterrupt = true;
}

//******************************** QUADCOPTER ********************************
Quadcopter drone;

//******************************** COM ********************************
#define CHECK_CONNECTION_DT 200
char car;
String input;
boolean stat=false;
void computeCmd();
bool isCmd(String &str,char *cmd);
unsigned long connectionCheck;
String getArg(String &str,int arg=0);

bool BlinkState=false;

//******************************** SETUP ********************************
void setup() 
{
  
  pinMode(13,OUTPUT);
  
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    Wire.begin();
    Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
    Fastwire::setup(400, true);
  #endif
  
  Serial.begin(115200);
  
  mpu.initialize();
  pinMode(INTERRUPT_PIN, INPUT);
  devStatus = mpu.dmpInitialize();
  mpu.setXGyroOffset(16);
  mpu.setYGyroOffset(-244);
  mpu.setZGyroOffset(102);
  //mpu.setZAccelOffset(1788);
  if (devStatus == 0) {
    mpu.setDMPEnabled(true);
    attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
    mpuIntStatus = mpu.getIntStatus();
    dmpReady = true;
    packetSize = mpu.dmpGetFIFOPacketSize();
  }
  else{
    Serial.print(F("DMP Initialization failed (code "));
    Serial.print(devStatus);
    Serial.println(F(")"));
  }
  drone.initTime();
  connectionCheck=millis()+CHECK_CONNECTION_DT;
}

//******************************** LOOP ********************************
void loop(){
  
  if (!dmpReady) return;
  // wait for MPU interrupt or extra packet(s) available
  while (!mpuInterrupt && fifoCount < packetSize) {
    computeCmd();
  }
  
  mpuInterrupt = false;
  mpuIntStatus = mpu.getIntStatus();
  fifoCount = mpu.getFIFOCount();
  if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
    mpu.resetFIFO();
  }
  else if (mpuIntStatus & 0x02){
    while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();
    
    mpu.getFIFOBytes(fifoBuffer, packetSize);
    fifoCount -= packetSize;
    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetGravity(&gravity, &q);
    mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
    /*Serial.print("ypr\t");
    Serial.print(ypr[0] * 180/M_PI);
    Serial.print("\t");
    Serial.print(ypr[1] * 180/M_PI);
    Serial.print("\t");
    Serial.println(ypr[2] * 180/M_PI);*/
    //Serial.println(ypr[1] * 180/M_PI);
    drone.compute(ypr[2] * 180/M_PI,ypr[1] * 180/M_PI);
  }
  BlinkState=!BlinkState;
  digitalWrite(13,BlinkState);
  Serial.println(micros());
  
}
//******************************** COM ********************************
void computeCmd(){
  if(connectionCheck<=millis())
    drone.setPowerOff(true);
  if(Serial.available()){
    input="";
    car=Serial.read();
    do{
      input+=car;
      while(!Serial.available());
      car=Serial.read();
    }while(car!='\n');
    if(isCmd(input,"c")){
      connectionCheck=millis()+CHECK_CONNECTION_DT;
    }
    else if(isCmd(input,"Px")){
      drone.getPIDX()->kp=getArg(input).toFloat();
    }
    else if(isCmd(input,"Ix")){
      drone.getPIDX()->ki=getArg(input).toFloat();
    }
    else if(isCmd(input,"Dx")){
      drone.getPIDX()->kd=getArg(input).toFloat();
    }
    else if(isCmd(input,"Py")){
      drone.getPIDY()->kp=getArg(input).toFloat();
    }
    else if(isCmd(input,"Iy")){
      drone.getPIDY()->ki=getArg(input).toFloat();
    }
    else if(isCmd(input,"Dy")){
      drone.getPIDY()->kd=getArg(input).toFloat();
    }
    else if(isCmd(input,"G")){
      drone.setPower(getArg(input).toInt());
    }
    else if(isCmd(input,"Cx")){
      drone.getPIDX()->setpoint=getArg(input).toFloat();
    }
    else if(isCmd(input,"Cy")){
      drone.getPIDY()->setpoint=getArg(input).toFloat();
    }
    else if(isCmd(input,"S") || isCmd(input,"s")){
      drone.setPowerOff(true);
    }
    else if(isCmd(input,"R")){
      drone.setPowerOff(false);
    }
  }
}
bool isCmd(String &str,char *cmd){
  unsigned int i;
  for(i=0;cmd[i]!='\0' && str[i]!='\0';i++){
    if(str[i]!=cmd[i])
      return false;
  }
  if(str[i]=='\0')
    return cmd[i]=='\0';
  return str[i]==' ';
}
String getArg(String &str,int arg){
  String value="";
  unsigned int i=0;
  do{
    for(;str[i]!='\0' && str[i]!=' ';i++);
    arg--;
  }while(arg>-1 && str[i]!='\0');
  i++;
  for(;str[i]!='\0' && str[i]!=' ';i++){
    value+=str[i];
  }
  return value;
}

