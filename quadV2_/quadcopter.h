#include "pid.h"
#include <Arduino.h>

#define MOTOR_COUNT 2
#define MOTOR_MIN 1000
#define MOTOR_MAX 2000

class Quadcopter{
public:
  Quadcopter(){
    power=MOTOR_MIN;
    powerOff=false;
    stat=false;
    DDRB|=0x3e;//pin 13,11,10,9 et 8 configure en sortie
  }
  void initTime(){
    time=micros();
  }
  void compute(float angle){
    unsigned int i;
    dt=(float)(micros()-time)/1000000L;
    time=micros();
    pidX.compute(angle,dt);
    esc[0]=power+pidX.getOutput();
    esc[1]=power-pidX.getOutput();
  
    if(!powerOff){
      Serial.print(angle-pidX.setpoint);Serial.print("\t");Serial.println(pidX.getOutput());
      pulseStart=micros();
      PORTB|=0x0a;
      for(i=0;i<MOTOR_COUNT;i++){
        if(esc[i]<MOTOR_MIN)
          esc[i]=MOTOR_MIN;
        else if(esc[i]>MOTOR_MAX)
          esc[i]=MOTOR_MAX;
        escPulse[i]=pulseStart+esc[i];
      }
      while((PORTB&0x0a)!=0){
        if(escPulse[0]<=micros())
          PORTB&=0xfd;
        if(escPulse[1]<=micros())
          PORTB&=0xf7;
      }

      //PORTB=(PORTB&0x1f)|0b100000;
      PORTB=(PORTB&0x1f)|(stat?0x20:0x00);
      stat=!stat;
    }
    else{
      PORTB&=0x1f;
    }
  }
  PID* getPID(){
    return &pidX;
  }
  void setPower(int p){
    power=p;
    if(power<MOTOR_MIN)
      power=MOTOR_MIN;
    else if(power>MOTOR_MAX)
      power=MOTOR_MAX;
  }
  void setPowerOff(bool po){
    powerOff=po;
  }
protected:
  PID pidX;
  unsigned long time,pulseStart,escPulse[MOTOR_COUNT];
  int power,esc[MOTOR_COUNT];
  float dt;
  boolean powerOff,stat;
};

