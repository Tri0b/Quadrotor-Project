PVector center;//variables graphique
float aspect;
float[] x={0,0,0};//base b3
float[] y={0,0,0};
float[] z={0,0,0};
float[] alpha={0,0};//inconue
float[] beta={0,0};
float[] teta={0,0};
float motor[]={2,3,2.5,2};//pousse des moteurs
float integ1=0,integ2=0;
float time=0,dt=0.02,d=0.1,A=0.2,B=0.2,C=0.3;//constante
PVector[] quad={new PVector(-10,-200,10),new PVector(10,-200,10),new PVector(10,200,10),new PVector(-10,200,10),//modele 3d du quadricopter
                new PVector(-10,-200,-10),new PVector(10,-200,-10),new PVector(10,200,-10),new PVector(-10,200,-10),
                new PVector(10,-200,10),new PVector(10,-200,-10),new PVector(10,200,-10),new PVector(10,200,10),
                new PVector(-10,-200,10),new PVector(-10,-200,-10),new PVector(-10,200,-10),new PVector(-10,200,10),
                new PVector(10,-200,10),new PVector(10,-200,-10),new PVector(-10,-200,-10),new PVector(-10,-200,10),
                new PVector(10,200,10),new PVector(10,200,-10),new PVector(-10,200,-10),new PVector(-10,200,10),
                new PVector(-200,-10,10),new PVector(-200,10,10),new PVector(200,10,10),new PVector(200,-10,10),
                new PVector(-200,-10,-10),new PVector(-200,10,-10),new PVector(200,10,-10),new PVector(200,-10,-10),
                new PVector(-200,10,10),new PVector(-200,10,-10),new PVector(200,10,-10),new PVector(200,10,10),
                new PVector(-200,-10,10),new PVector(-200,-10,-10),new PVector(200,-10,-10),new PVector(200,-10,10),
                new PVector(-200,10,10),new PVector(-200,10,-10),new PVector(-200,-10,-10),new PVector(-200,-10,10),
                new PVector(200,10,10),new PVector(200,10,-10),new PVector(200,-10,-10),new PVector(200,-10,10)};
boolean started=false;
String[] waitText={"contrôle des gaz avec la souris (sans asservissement)","placer la souris au centre de la fenêtre avant de commencer","appuyer sur espace pour commencer"};

void setup(){
  size(1000, 800, P3D);

  center = new PVector(width/2, height/2);
  aspect = (float)width / (float)height;
}

float projection(PVector v,int dim){
  return x[dim]*v.x+y[dim]*v.y+z[dim]*v.z;
}

void calculate(){
  integ1+=d*(motor[2]-motor[0])*dt/B;
  integ2+=d*(motor[1]-motor[3])*dt/A;
  if((beta[0]%PI)==0){
    alpha[1]=integ1;
    teta[1]=0;
    beta[1]=integ2+teta[1]*sin(alpha[0]);
  }else if((abs(alpha[0])%PI)==(PI/2)){
    println("impossible de calculer beta et teta (alpha=+-pi/2)");
  }else{
    teta[1]=integ1*sin(beta[0])/cos(alpha[0]);
    alpha[1]=teta[1]*cos(alpha[0])*cos(beta[0])/sin(beta[0]);
    beta[1]=integ2+teta[1]*sin(alpha[0]);
  }
  
  
  alpha[0]+=alpha[1]*dt;
  beta[0]+=beta[1]*dt;
  teta[0]+=teta[1]*dt;
  time+=dt;
}

void drawQuad(){
  /*x[0]=cos(teta[0])*cos(beta[0]);
  x[1]=cos(teta[0])*sin(beta[0])*sin(alpha[0])+sin(teta[0])*cos(alpha[0]);
  x[2]=-cos(teta[0])*sin(beta[0])*cos(alpha[0])+sin(teta[0])*sin(alpha[0]);
  y[0]=-sin(teta[0])*cos(beta[0]);
  y[1]=cos(teta[0])*cos(alpha[0])-sin(teta[0])*sin(beta[0])*sin(alpha[0]);
  y[2]=cos(teta[0])*sin(alpha[0])+sin(teta[0])*sin(beta[0])*cos(alpha[0]);
  z[0]=sin(beta[0]);
  z[1]=-cos(beta[0])*sin(alpha[0]);
  z[2]=cos(beta[0])*cos(alpha[0]);*/
  x[0]=cos(alpha[0])*cos(teta[0]);
  x[1]=-cos(beta[0])*sin(teta[0])+sin(beta[0])*sin(alpha[0])*cos(teta[0]);
  x[2]=cos(beta[0])*sin(alpha[0])*cos(teta[0])+sin(beta[0])*sin(teta[0]);
  y[0]=cos(alpha[0])*sin(teta[0]);
  y[1]=cos(beta[0])*cos(teta[0])+sin(beta[0])*sin(alpha[0])*sin(teta[0]);
  y[2]=cos(beta[0])*sin(alpha[0])*sin(teta[0])-sin(beta[0])*cos(teta[0]);
  z[0]=-sin(alpha[0]);
  z[1]=sin(beta[0])*cos(alpha[0]);
  z[2]=cos(beta[0])*cos(alpha[0]);
  float scale=400;
  beginShape(LINES);
  stroke(255,0,0);
  vertex(0,0,0);
  vertex(x[0]*scale,x[1]*scale,x[2]*scale);
  stroke(0,255,0);
  vertex(0,0,0);
  vertex(y[0]*scale,y[1]*scale,y[2]*scale);
  stroke(0,0,255);
  vertex(0,0,0);
  vertex(z[0]*scale,z[1]*scale,z[2]*scale);
  endShape();
  noStroke();
  fill(127,127,127);
  beginShape(QUADS);
  for(int i=0;i<quad.length;i++){
    vertex(projection(quad[i],0),projection(quad[i],1),projection(quad[i],2));
  }
  endShape();
}

void drawFloor(){
  noStroke();
  fill(121,192,34);
  beginShape();
  vertex(400, 400, -400);
  vertex(-400, 400, -400);
  vertex(-400, -400, -400);
  vertex(400, -400, -400);
  endShape(CLOSE);
}

void draw(){
  if(keyPressed && !started){
    if(key==' '){
      started=true;
    }
  }
  background(0);
  lights();
  perspective(PI/2, aspect, 1, 10000);
  pushMatrix();
  translate(center.x, center.y);
  float y=map(mouseY, 0, height, -10, 10),x=map(mouseX, 0, width, -10, 10),mean=15;
  motor[0]=mean+x;
  motor[1]=mean+y;
  motor[2]=mean-x;
  motor[3]=mean-y;
  /*alpha[0]=map(mouseY, 0, height, -PI/2, PI/2);
  beta[0]=map(mouseX, 0, width, -PI/2, PI/2);*/
  rotateX(PI/4);
  rotateZ(teta[0]);
  noStroke();
  fill(127,127,127);
  pushMatrix();
  if(started){
    calculate();
  }
  drawQuad();
  drawFloor();
  popMatrix();
  popMatrix();
  perspective();
  stroke(255);
  fill(255);
  textSize(20);
  text("alpha: "+alpha[0],10,20);
  text("beta:  "+beta[0],10,40);
  text("teta:  "+teta[0],10,60);
  text("t:     "+time,10,80);
  if(!started){
    textSize(25);
    stroke(255);
    fill(255);
    for(int i=0;i<waitText.length;i++){
      text(waitText[i],(width-textWidth(waitText[i]))/2,200+25*i);
    }
    /*text("contrôle des gaz avec la sourie (sans asservissement)",(width-textWidth("appuyer sur espace pour commencer"))/2,200);
    text("appuyer sur espace pour commencer",(width-textWidth("appuyer sur espace pour commencer"))/2,200);
    text("appuyer sur espace pour commencer",(width-textWidth("appuyer sur espace pour commencer"))/2,200);*/
  }
}