#! /usr/bin/env python
#################################################################################
#     File Name           :     MyGUI.py
#     Created By          :     robin
#     Creation Date       :     [2017-05-29 03:02]
#     Last Modified       :     [2017-05-29 15:16]
#     Description         :      
#################################################################################









import numpy as np

import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons

from cmath import *

def Talbot(F,t,N): #Calcul la transformée inverse avec la méthode Talbot à l'instant t de F sur N itérations
    
#   Initiate the stepsize
    h = 2*pi/N;
  
#   Shift contour to the right in case there is a pole on the positive real axis : Note the contour will
#   not be optimal since it was originally devoloped for function with
#   singularities on the negative real axis
#   For example take F(s) = 1/(s-1), it has a pole at s = 1, the contour needs to be shifted with one
#   unit, i.e shift  = 1. But in the test example no shifting is necessary

    shift = 0.0;
    ans =   0.0;
    
    if t == 0:
        print ("ERROR:   Inverse transform can not be calculated for t=0")
        return ("Error");
        
#   The for loop is evaluating the Laplace inversion at each point theta which is based on the trapezoidal   rule
    for k in range(0,N):
        theta = -pi + (k+1./2)*h;
        z = shift + N/t*(0.5017*theta*cot(0.6407*theta) - 0.6122 + 0.2645j*theta); 
        dz = N/t*(-0.5017*0.6407*theta*(csc(0.6407*theta)**2)+0.5017*cot(0.6407*theta)+0.2645j);
        ans = ans + exp(z*t)*F(z)*dz;
        
    return ((h/(2j*pi))*ans).real        
      

def cot(phi):
    return 1.0/tan(phi)

def csc(phi):
    return 1.0/sin(phi)









#TEST DIRECT AUTRE METHODE D INVERSIOn



# Based on "Approximate Inversion of the Laplace Transform" by Cheng and Sidauruk, 
# in the Mathematica Journal, vol 4, issue 2, 1994
import scipy.misc

fact = scipy.misc.factorial

def csteh(n, i):
    acc = 0.0
    for k in range(int(np.floor((i+1)/2.0)), int(min(i, n/2.0))+1):
        num = k**(n/2.0) * fact(2 * k)
        den = fact(i - k) * fact(k -1) * fact(k) * fact(2*k - i) * fact(n/2.0 - k)
        acc += (num /den)
    expo = i+n/2.0
    term = np.power(-1+0.0j,expo)
    res = term * acc
    return res.real

def nlinvsteh(F, t, n = 6):
    acc = 0.0
    lton2 = np.log(2) / t
    for i in range(1, n+1):
        a = csteh(n, i)
        b = F(i * lton2)
        acc += (a * b)
    return lton2 * acc



#FIN METHODE















#Definitions


    #Mon système

Consigne=5

P,I,D=10,0,0

A= 8040 # 0.00804 Gain ESC V/µs = 8040 
Kv= 188 #rad/s/V Constante moteur
d=6.49E-7 # N.m.s²/rad²   constante couple helice/air  

#Axe z
Iz= 0.033
Psi_0=0 #Conditions initiales
dPsi_0=0


def PID(p):
    return P*(1+I/p+D*p)

def PFD(p):
    return d/(Iz*(p**2-p*Psi_0-Psi_0))



def E(p):
    return Consigne/p

def H(p):
    global P,I,D
    return 1000*A*Kv*d*PID(p)*PFD(p)/(1+1000*A*Kv*d*PID(p)*PFD(p))
    #return 100*PID(p)*PFD(p)/(1+100*PFD(p)*PID(p))
    #return 1/(1+p)
    
def S(p):
    return E(p)*H(p)

 


#Domaine temporel
def Inverse_Laplace(F,t,N=70):
    S_n=[]
    for i in t:
        #S_n.append(Talbot(S,i,N)) #Au dela d'environ 70 ça crash
        S_n.append(nlinvsteh(S, i))
    return S_n        



#Domaine fréquentiel


def Gain_dB(H,f_n):

    GdB=[]
    for i in f_n:
        GdB.append(20*np.log10(np.abs(H(1j*i))))

    return GdB



#Plotting


t0=0.01
tf=2

tn=np.linspace(t0,tf,200)
Puls_n=np.linspace(1,1000,500)



fig, axarr = plt.subplots(2,1)
plt.subplots_adjust(left=0.25, bottom=0.25)



L1, = axarr[0].plot(tn, Inverse_Laplace(S,tn), lw=2, color='red')

L2, = axarr[1].plot(Puls_n,Gain_dB(H,Puls_n), lw=2, color='red')




#plt.axis([0, 1, -10, 10])

axarr[0].axis([t0,tf,0,10])

axarr[1].axis([1,1000,-60,0])
axarr[1].set_xscale('log')


axcolor = 'lightgoldenrodyellow'
axP = plt.axes([0.25, 0.18, 0.65, 0.03], facecolor=axcolor)
axI = plt.axes([0.25, 0.14, 0.65, 0.03], facecolor=axcolor)
axD = plt.axes([0.25, 0.10, 0.65, 0.03], facecolor=axcolor)

sP = Slider(axP, 'P', 0.01, 500, valinit=10)
sI = Slider(axI, 'I', 0, 5, valinit=0)
sD = Slider(axD, 'D', 0, 20, valinit=0)



def update(val):
    global P,I,D
    P=sP.val
    I=sI.val
    D=sD.val
    L1.set_ydata(Inverse_Laplace(S,tn))
    L2.set_ydata(Gain_dB(H,Puls_n))
    fig.canvas.draw_idle()
sP.on_changed(update)
sI.on_changed(update)
sD.on_changed(update)

resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')


def reset(event):
    sP.reset()
    sI.reset()
    sD.reset()

button.on_clicked(reset)

rax = plt.axes([0.025, 0.5, 0.15, 0.15], facecolor=axcolor)
radio = RadioButtons(rax, ('red', 'blue', 'green'), active=0)


def colorfunc(label):
    L1.set_color(label)
    fig.canvas.draw_idle()
radio.on_clicked(colorfunc)


plt.plot()


plt.show()