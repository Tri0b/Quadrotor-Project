import numpy as np
import cv2

cap = cv2.VideoCapture(0)

while(True):
	# Capture frame-by-frame
	ret, frame = cap.read()
	image=frame.copy()
	#gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	hsv = cv2.cvtColor(image , cv2.COLOR_BGR2HSV)
	min=np.array([0,200,150])
	max=np.array([20,255,255])
	gray=cv2.inRange(hsv,min,max)

	circles = cv2.HoughCircles(gray,cv2.HOUGH_GRADIENT,1,500,param1=100,param2=20,minRadius=0,maxRadius=0)
	# ensure at least some circles were found
	if circles is not None:
		# convert the (x, y) coordinates and radius of the circles to integers
		circles = np.round(circles[0, :]).astype("int")

		# loop over the (x, y) coordinates and radius of the circles
		for (x, y, r) in circles:
			# draw the circle in the output image, then draw a rectangle
			# corresponding to the center of the circle
			cv2.circle(image, (x, y), r, (0, 255, 0), 4)
			cv2.rectangle(image, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
	
	cv2.imshow('frame',image)
	key=cv2.waitKey(1) & 0xFF
	if key == ord('q'):
		break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()