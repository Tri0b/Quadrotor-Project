
//ALL CREDITS TO https://github.com/zendes/SBUS


// The Arduino Pro Mini is a simple & cheap board, ideal for leaving inside of
// your model. It only has a single Serial port, and the default SBUS code
// doesn't run on it. You can get it to work by not using the timer, but that
// means you can't use delay in the loop() function anymore.

#include <SBUS.h>
SBUS sbus(Serial);


class R9D
{
  public:
    R9D(SBUS *sbus);

    void Update();
    void Set_Commands(int *Commands);
    int Get_Left_Joystick_Horizontal();
    int Get_Left_Joystick_Vertical();
    int Get_Right_Joystick_Horizontal();
    int Get_Right_Joystick_Vertical();
    int Get_SwitchA();
    int Get_SwitchB();
    int Get_SwitchC();
    int Get_VRC();
    int Get_VRB();
    
  private:
    //int m_Throttle;
    int m_Left_Joystick_Horizontal;
    int m_Left_Joystick_Vertical; //Throttle
    int m_Right_Joystick_Horizontal;
    int m_Right_Joystick_Vertical;
    int m_SwitchA;
    int m_SwitchB;
    int m_SwitchC;
    int m_VRC; // (Left POTAR)
    int m_VRB; // (RIGHT POTAR)

    SBUS *m_sbus; // [0 - 3] Joysticks , [4 - 6] Switchs , [7 - 8] Knobs
};

R9D::R9D(SBUS *sbus)
{
    m_Left_Joystick_Horizontal=0;
    m_Left_Joystick_Vertical=0; //Throttle
    m_Right_Joystick_Horizontal=0;
    m_Right_Joystick_Vertical=0;
    m_SwitchA=0;
    m_SwitchB=0;
    m_SwitchC=0;
    m_VRC=0; // (Left POTAR)
    m_VRB=0; // (RIGHT POTAR)

    m_sbus=sbus;
}

void R9D::Update()
{
  m_sbus->process();
  m_Left_Joystick_Horizontal=(m_sbus->getChannel(4))/8; //The 8 divider is requiered
  m_Left_Joystick_Vertical=(m_sbus->getChannel(3))/8; //Throttle
  m_Right_Joystick_Horizontal=(m_sbus->getChannel(1))/8;
  m_Right_Joystick_Vertical=(m_sbus->getChannel(2))/8;
  m_SwitchA=(m_sbus->getChannel(9))/8;
  m_SwitchB=0; //TODO: Check the chan of Switch B ^^"
  m_SwitchC=(m_sbus->getChannel(5))/8;
  m_VRC=(m_sbus->getChannel(7))/8; // (Left POTAR)
  m_VRB=(m_sbus->getChannel(8))/8; // (RIGHT POTAR)
}

void R9D::Set_Commands(int *Commands)
{
  Commands[0]=m_Left_Joystick_Horizontal;
  Commands[1]=m_Left_Joystick_Vertical; //Throttle
  Commands[2]=m_Right_Joystick_Horizontal;
  Commands[3]=m_Right_Joystick_Vertical;
  Commands[4]=m_SwitchA;
  Commands[5]=m_SwitchB;
  Commands[6]=m_SwitchC;
  Commands[7]=m_VRC; // (Left POTAR)
  Commands[8]=m_VRB=0; // (RIGHT POTAR)
}

int R9D::Get_Left_Joystick_Horizontal()
{
  return m_Left_Joystick_Horizontal;
}

int R9D::Get_Left_Joystick_Vertical()
{
  return m_Left_Joystick_Vertical;
}

int R9D::Get_Right_Joystick_Horizontal()
{
  return m_Right_Joystick_Horizontal;
}

int R9D::Get_Right_Joystick_Vertical()
{
  return m_Right_Joystick_Vertical;
}

int R9D::Get_SwitchA()
{
  return m_SwitchA;
}

int R9D::Get_SwitchB()
{
  return m_SwitchB;
}
int R9D::Get_SwitchC()
{
  return m_SwitchC;
}

int R9D::Get_VRC()
{
  return m_VRC;
}

int R9D::Get_VRB()
{
  return m_VRB;
}


R9D Receiver(&sbus);

int Radio[9];
void setup()
{
  pinMode(11,OUTPUT);
  sbus.begin(false);
}

void loop()
{
  Receiver.Update();
  Receiver.Set_Commands(Radio);
  analogWrite(11,Radio[0]);
}
