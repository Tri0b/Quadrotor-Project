#ifndef PID_H
#define PID_H

#include <Arduino.h>

class PID
{
  public:
    PID(float * Data,unsigned long *Clock=NULL);
    void Set_PID(float P,float I,float D,float N=0);
    float Get_Response();
    
  private:
    float m_Order; //The value we want to reach
    float *m_Data; //The Data to control (simple float, no array)
    float m_Error;
    float m_Prev_Error;
    unsigned long *m_Clock;

    float m_Integrator;
    
    float m_P;
    float m_I;
    float m_D;
    float m_N;// Not used yet

    float m_Offset;

    
    float m_Max; //Both egals means no limit
    float m_Min;
    
};


#endif
