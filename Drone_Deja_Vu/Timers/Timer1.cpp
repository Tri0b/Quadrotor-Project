#include <Arduino.h>
#include "Timer1.h"

//Setup du Timer 1 (16 bits )
//Ce timer est utilisé par la Servo.h et les PWM 9 et 10.
volatile unsigned long TIMER1=0;
volatile bool State=false;



void Setup_Timer1_Normal() //With CTC
{
  cli();
  
  
  TCCR1A=0; 
  TCCR1B=0;
  
  //TCCR1B|=(0<<CS12);
  TCCR1B|=(1<<CS11); //Prescalers réglés sur 8
  //TCCR1B|=(1<<CS10); 

  TCCR1B |= (1 << WGM12); // Active le mode CTC: Clear Timer on Compare. Quand l'égalité est vérifiée, le timer est clear.
  
  OCR1A=99; // T= 1E-4s
  
  //TIMSK1|=(1<<TOIE1); //Active les intérruptions générées par dépassement
  TIMSK1 |= (1 << OCIE1A);
  sei();
}

unsigned long Get_Timer1()
{
  return TIMER1;
}


ISR(TIMER1_COMPA_vect) 
{
  
  State=!State;

  if(State)
  {
    TIMER1++;
    digitalWrite(13,HIGH);

  }
  else
  {
    digitalWrite(13,LOW);
  }
}

