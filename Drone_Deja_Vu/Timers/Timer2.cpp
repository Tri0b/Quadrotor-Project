#include <Arduino.h>
#include "Timer2.h"


void Setup_Timer2_Fast_PWM()
{
  cli();
  TCCR2B&= ~(1<<WGM22); //FAST PWM NON INVERTING
  TCCR2A|=  (1<<WGM21);
  TCCR2A|=  (1<<WGM20);

  TCCR2B=0;
  TCCR2B=(1<<CS22); //Prescale of 256

  TIMSK2=(1<<TOIE2);//Enable interuption on Timer Overflow 


  OCR2A=0;
  OCR2B=0;
  
  sei();
}


void Clear_COA2_On_Match()
{
  TCCR2A|=  (1<<COM2A1);//Clear COA2 on match
  TCCR2A&= ~(1<<COM2A0);
}

void Disable_COA2()
{
  TCCR2A&= ~(1<<COM2A1);
  TCCR2A&= ~(1<<COM2A0);
}

void Clear_COB2_On_Match()
{
  TCCR2A|=  (1<<COM2B1);//Clear COB2 on match
  TCCR2A&= ~(1<<COM2B0);
}

void Disable_COB2()
{
  TCCR2A&= ~(1<<COM2B1);
  TCCR2A&= ~(1<<COM2B0);
}

void Set_Duty_Cycle2A(uint8_t Value)
{
  OCR2A=Value;
}

void Set_Duty_Cycle2B(uint8_t Value)
{
  OCR2B=Value;
}


ISR(TIMER2_OVF_vect)
{
  if(TCCR2A & (1<<COM2A0) != 0 && TCCR2A & (1<<COM2A1) != 0)
  {
    digitalWrite(6,HIGH);
  }

  if(TCCR2A & (1<<COM2B0) != 0 && TCCR2A & (1<<COM2B1) != 0)
  {
    digitalWrite(5,HIGH);
  }

}



