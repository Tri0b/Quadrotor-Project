#include <Arduino.h>
#include "Timer0.h"


void Setup_Timer0_Fast_PWM()
{
  cli();
  TCCR0B&= ~(1<<WGM02); //FAST PWM NON INVERTING
  TCCR0A|=  (1<<WGM01);
  TCCR0A|=  (1<<WGM00);

  TCCR0B=0;
  TCCR0B=(1<<CS02); //Prescale of 256

  TIMSK0=(1<<TOIE0);//Enable interuption on Timer Overflow 


  OCR0A=0;
  OCR0B=0;
  
  sei();
}


void Clear_COA0_On_Match()
{
  TCCR0A|=  (1<<COM0A1);//Clear COA0 on match
  TCCR0A&= ~(1<<COM0A0);
}

void Disable_COA0()
{
  TCCR0A&= ~(1<<COM0A1);
  TCCR0A&= ~(1<<COM0A0);
}

void Clear_COB0_On_Match()
{
  TCCR0A|=  (1<<COM0B1);//Clear COB0 on match
  TCCR0A&= ~(1<<COM0B0);
}

void Disable_COB0()
{
  TCCR0A&= ~(1<<COM0B1);
  TCCR0A&= ~(1<<COM0B0);
}

void Set_Duty_Cycle0A(uint8_t Value)
{
  OCR0A=Value;
}

void Set_Duty_Cycle0B(uint8_t Value)
{
  OCR0B=Value;
}

ISR(TIMER0_OVF_vect)
{
  if(TCCR0A & (1<<COM0A0) != 0 && TCCR0A & (1<<COM0A1) != 0)
  {
    digitalWrite(6,HIGH);
  }

  if(TCCR0A & (1<<COM0B0) != 0 && TCCR0A & (1<<COM0B1) != 0)
  {
    digitalWrite(5,HIGH);
  }

}

