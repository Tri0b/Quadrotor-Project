#ifndef TIMER2_H
#define TIMER2_H


void Setup_Timer2_Fast_PWM();
void Clear_COA2_On_Match();
void Disable_COA2();
void Clear_COB2_On_Match();
void Disable_COB2();

void Set_Duty_Cycle2A(uint8_t Value);
void Set_Duty_Cycle2B(uint8_t Value);

#endif TIMER2_H
