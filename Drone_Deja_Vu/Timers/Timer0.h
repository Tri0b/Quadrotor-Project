#ifndef TIMER0_H
#define TIMER0_H

void Setup_Timer0_Fast_PWM();


void Clear_COA0_On_Match();

void Disable_COA0();

void Clear_COB0_On_Match();

void Disable_COB0();

void Set_Duty_Cycle0A(uint8_t Value);
void Set_Duty_Cycle0B(uint8_t Value);

#endif


