#include "PID.h"

PID::PID(float *Order,float *Data,unsigned long *Clock=NULL)
{
  m_Order=Order;
  m_Data=Data;
  m_Error=0.0;
  m_Prev_Error=0.0;
  m_Clock=Clock;

  m_Integrator=0.0;
  
  m_P=0.0;
  m_I=0.0;
  m_D=0.0;
  m_N=0.0;

  m_Offset=0.0;
  
  m_Max=0.0;
  m_Min=0.0;
}

void PID::Set_PID(float P,float I,float D,float N=0)
{
  m_P=P;
  m_I=I;
  m_D=D;
  m_N=N;
}

float PID::Get_Response()
{
  m_Error=*m_Order- *m_Data;
  float dt=((float)*m_Clock)/1000.0;
  
  if(m_I==0.0)
  {
    m_Integrator=0.0;
  }
  else
  {
    m_Integrator+=m_I*m_Error*dt;
  }
  float Response=m_P*(m_Error+m_Integrator+m_D*(m_Error-m_Prev_Error)/dt)+m_Offset;

  if(m_Max != m_Min &&  m_Max > m_Min)
  {
    if(Response>m_Max)
    {
      Response=m_Max;
    }

    if(Response < m_Min)
    {
      Response=m_Min;
    }
  }
  
  m_Prev_Error=m_Error;
  return Response;
}
