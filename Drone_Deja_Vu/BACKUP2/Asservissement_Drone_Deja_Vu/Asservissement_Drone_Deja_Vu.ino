//#include <SPI.h>
#include <Wire.h>


#include <math.h>

#include "CF1.h"
#include "PID.h"
#include "R9D.h"



//#define DEBUG


/* This driver uses the Adafruit unified sensor library (Adafruit_Sensor),
   which provides a common 'type' for sensor data and some helper functions.
   
   To use this driver you will also need to download the Adafruit_Sensor
   library and include it in your libraries folder.

   You should also assign a unique ID to this sensor for use with
   the Adafruit Sensor API so that you can identify this particular
   sensor in any data logs, etc.  To assign a unique ID, simply
   provide an appropriate value in the constructor below (12345
   is used by default in this example).
   
   Connections (For default I2C)
   ===========
   Connect SCL to analog 5
   Connect SDA to analog 4
   Connect VDD to 5V DC
   Connect GROUND to common ground

   History
   =======
   2014/JULY/25  - First version (KTOWN)
*/
   
/* Assign a unique base ID for this sensor */   
Adafruit_LSM9DS0 lsm = Adafruit_LSM9DS0(1000);  // Use I2C, ID #1000


/* Or, use Hardware SPI:
  SCK -> SPI CLK
  SDA -> SPI MOSI
  G_SDO + XM_SDO -> tied together to SPI MISO
  then select any two pins for the two CS lines:
*/

#define LSM9DS0_XM_CS 10
#define LSM9DS0_GYRO_CS 9
//Adafruit_LSM9DS0 lsm = Adafruit_LSM9DS0(LSM9DS0_XM_CS, LSM9DS0_GYRO_CS, 1000);

/* Or, use Software SPI:
  G_SDO + XM_SDO -> tied together to the MISO pin!
  then select any pins for the SPI lines, and the two CS pins above
*/

#define LSM9DS0_SCLK 13
#define LSM9DS0_MISO 12
#define LSM9DS0_MOSI 11

//Adafruit_LSM9DS0 lsm = Adafruit_LSM9DS0(LSM9DS0_SCLK, LSM9DS0_MISO, LSM9DS0_MOSI, LSM9DS0_XM_CS, LSM9DS0_GYRO_CS, 1000);


/**************************************************************************/
/*
    Displays some basic information on this sensor from the unified
    sensor API sensor_t type (see Adafruit_Sensor for more information)
*/
/**************************************************************************/






/**************************************************************************/
/*
    Configures the gain and integration time for the TSL2561
*/
/**************************************************************************/
void configureSensor(void)
{
  
  // 1.) Set the accelerometer range
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_2G);
  lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_4G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_6G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_8G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_16G);
  
  // 2.) Set the magnetometer sensitivity
  lsm.setupMag(lsm.LSM9DS0_MAGGAIN_2GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_4GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_8GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_12GAUSS);

  // 3.) Setup the gyroscope
  //lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_245DPS);
  //lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_500DPS);
  lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_2000DPS);
}

/**************************************************************************/
/*
    Arduino setup function (automatically called at startup)
*/
/**************************************************************************/

sensors_event_t accel, mag, gyro, temp;
unsigned long Clock=0;
unsigned long previous;
unsigned long dt=0;
float ypr[3];
CF1 AngleFilter(&dt,0.96);





#ifndef DEBUG
SBUS sbus(Serial);
R9D Receiver(&sbus);
#endif

int Radio[9];

float C_Pitch=0.0;
float C_Roll= 0.0;
float C_Yaw=0.0;

float R_Throttle=0.0; 
float R_Yaw=0.0;
float R_Pitch=0.0;
float R_Roll=0.0;

PID PID_Pitch(&R_Pitch,&(ypr[1]),&dt);
PID PID_Roll(&R_Roll,&(ypr[2]),&dt);
PID PID_Yaw(&R_Yaw,&(gyro.gyro.z),&dt);


void Disp_CSV()
{
  Serial.println(String(millis())+";"+String(AngleFilter.Get_Angle(0))+";"+String(AngleFilter.Get_Angle(1))+";"+String(AngleFilter.Get_Angle(2))+";"+String(C_Pitch)+";"+String(C_Roll));
}


void setup(void) 
{

#ifdef DEBUG
  Serial.begin(115200);
#endif

#ifndef DEBUG
sbus.begin(false); 
#endif

  //Serial.println(F("LSM9DS0 9DOF Sensor Test")); Serial.println("");
  
  
  
  /* Initialise the sensor */
  if(!lsm.begin())
  {
    /* There was a problem detecting the LSM9DS0 ... check your connections */
    //Serial.print(F("Ooops, no LSM9DS0 detected ... Check your wiring or I2C ADDR!"));
    while(1);
  }
  //Serial.println(F("Found LSM9DS0 9DOF"));
  
  /* Display some basic information on this sensor */
  //displaySensorDetails();
  
  /* Setup the sensor gain and integration time */
  configureSensor();
  
  /* We're ready to go! */
  //Serial.println("");
  
  AngleFilter.Set_Container(ypr);
  AngleFilter.Set_Verbose(false);
  AngleFilter.Set_CSV(false);

  
  pinMode(3,OUTPUT);
  pinMode(5,OUTPUT);
  pinMode(6,OUTPUT);
  pinMode(10,OUTPUT);

  
  /*Motor_Pitch_1.attach(3);
  Motor_Pitch_2.attach(5);
  Motor_Roll_1.attach(6);
  Motor_Roll_2.attach(10);
  */


  
  PID_Pitch.Set_PID(0.0,0.0,0.0);
  PID_Roll.Set_PID (3.2,0.1,0.45);
  
  
  //PID_Yaw.Set_PID(2.0,0.0,0.0);
  PID_Yaw.Set_PID(0.0,0.0,0.0);
  /*
  PID_Pitch.Set_PID(0.0,0.0,0.0);
  PID_Roll.Set_PID (0.0,0.0,0.0);
  */

  

  /*
   * int Time_Init=millis();
   * while(millis()-Time_Init<4000)
  {
    digitalWrite(3,HIGH);
    digitalWrite(5,HIGH);
    digitalWrite(6,HIGH);
    digitalWrite(10,HIGH);
    delayMicroseconds(1000);
    digitalWrite(3,LOW);
    digitalWrite(5,LOW);
    digitalWrite(6,LOW);
    digitalWrite(10,LOW);
    delayMicroseconds(19000);
  }*/

  previous=millis();

}
void loop(void) 
{

  previous=Clock;
  Clock=millis();
  dt=Clock-previous;
  
  #ifndef DEBUG
  Receiver.Update(); //Get the radio commands from Serial
  Receiver.Set_Commands(Radio); //Filling "Radio"
  R_Throttle=8*Radio[1];
  R_Yaw=8*Radio[0];
  R_Pitch=8*Radio[2];
  R_Roll=8*Radio[3];

  R_Throttle=map(R_Throttle,300,1700,1000,1900);
  R_Roll=map(R_Roll,300,1700,-20,20);
  R_Pitch=-map(R_Pitch,300,1700,-20,20);
  R_Yaw=map(R_Yaw,300,1700,-50,50);
  #endif
  
  
  lsm.getEvent(&accel, &mag, &gyro, &temp);
  AngleFilter.Update(gyro.gyro,accel.acceleration);
  
  if(R_Throttle<=1100)
  {
    C_Pitch=0;
    C_Roll =0;
    C_Yaw  =0;
  }
  else
  {
    C_Pitch=PID_Pitch.Get_Response();
    C_Roll = PID_Roll.Get_Response();
    C_Yaw  = PID_Yaw.Get_Response();
  }

  digitalWrite(3,HIGH);
  digitalWrite(5,HIGH);
  digitalWrite(6,HIGH);
  digitalWrite(10,HIGH);
  
  unsigned long t=micros();
  
  unsigned long timeout1=t+constrain(R_Throttle+C_Pitch-C_Yaw,1100,2000),timeout2=t+constrain(R_Throttle-C_Pitch-C_Yaw,1100,2000),
                     timeout3=t+constrain(R_Throttle+C_Roll+C_Yaw,1100,2000),timeout4=t+constrain(R_Throttle-C_Roll+C_Yaw,1100,2000);
                     

                     
                     
  if(R_Throttle<=1100)
  {
    timeout1=t+1000;
    timeout2=t+1000;
    timeout3=t+1000;
    timeout4=t+1000;
  }

  do
  {
    if(t>=timeout1)
    {
      digitalWrite(3,LOW);
    }
    if(t>=timeout2)
    {
      digitalWrite(5,LOW);
    }
    if(t>=timeout3)
    {
      digitalWrite(6,LOW);
    }
    if(t>=timeout4)
    {
      digitalWrite(10,LOW);
    }
    t=micros();
  }while(t<timeout1 || t<timeout2 || t<timeout3 || t<timeout4);
  digitalWrite(3,LOW);
  digitalWrite(5,LOW);
  digitalWrite(6,LOW);
  digitalWrite(10,LOW);

  
  
    
  
  //Les données sont dans accel.acceleration.(x,y,z) ... gyro.gyro.(x,y,z) ... mag.magnetic.(x,y,z) ... temp.temperature
  //Les données sont de type float

  
}
