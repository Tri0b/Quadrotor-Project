#include <Arduino.h>
#include <math.h>
#include "CF1.h"

CF1::CF1(unsigned long *Clock,float Alpha)
{
  m_Clock=Clock;
  m_Alpha=Alpha;
  
  m_Filtered_Data[0]=0.0;
  m_Filtered_Data[1]=0.0;
  m_Filtered_Data[2]=0.0;

  /*m_Prev_Filtered_Data[0]=0.0;
  m_Prev_Filtered_Data[1]=0.0;
  m_Prev_Filtered_Data[2]=0.0;*/

  m_Gyro_ypr[0]=0.0;
  m_Gyro_ypr[1]=0.0;
  m_Gyro_ypr[2]=0.0;

  m_Acc_ypr[0]=0.0;
  m_Acc_ypr[1]=0.0;
  m_Acc_ypr[2]=0.0;

  m_p_Filtered=NULL;
  m_Verbose=false;
  m_CSV=false;
  m_Debug=false;


  //m_TestIntegrator=0.0;
}



void CF1::Set_Alpha(float Alpha)
{
  m_Alpha=Alpha;
}

void CF1::Set_Container(float Angle[3])
{
  m_p_Filtered=Angle;
}

/*void CF1::Update(float Gyro[3],float Acc[3])
{
  if(*m_Clock>0) //To do: Make smthing better than this for the timer overflow
  {
    //Compute Filtered_Data
    //float Yaw=  atan2(sqrt(pow(Acc[0],2)+pow(Acc[1],2)),Acc[2])*180/M_PI;
    m_Acc_ypr[0]= 0.0;
    m_Acc_ypr[1]=atan2(Acc[1],sqrt(pow(Acc[0],2)+pow(Acc[2],2)))*180/M_PI;
    m_Acc_ypr[2]= atan2(Acc[0],sqrt(pow(Acc[1],2)+pow(Acc[2],2)))*180/M_PI;

    m_Gyro_ypr[0]=Gyro[0]*((float)*m_Clock)/1000.0+m_Filtered_Data[0];
    m_Gyro_ypr[1]=Gyro[1]*((float)*m_Clock)/1000.0+m_Filtered_Data[1];
    m_Gyro_ypr[2]=Gyro[2]*((float)*m_Clock)/1000.0+m_Filtered_Data[2];
  
    //Update value
    m_Filtered_Data[0]= m_Gyro_ypr[0];
    m_Filtered_Data[1]= m_Alpha*(m_Gyro_ypr[1])+(1-m_Alpha)*m_Acc_ypr[1];
    m_Filtered_Data[2]= m_Alpha*(m_Gyro_ypr[2])+(1-m_Alpha)*m_Acc_ypr[2];

    

    if(m_p_Filtered!=NULL)
    {
      m_p_Filtered[0]=m_Filtered_Data[0];
      m_p_Filtered[1]=m_Filtered_Data[1];
      m_p_Filtered[2]=m_Filtered_Data[2];
    }

    Disp_Angle();
    Disp_CSV();
  }
  
}*/


void CF1::Update(sensors_vec_t Gyro,sensors_vec_t Acc)
{
  if(*m_Clock>0) //if no timer overflow
  {
     //Compute Filtered_Data
  
     //float Yaw=  atan2(sqrt(pow(Acc.x,2.0)+pow(Acc.y,2.0)),Acc.z)*180/M_PI;
    m_Acc_ypr[0]=0.0;  //accelerometer can't give yaw
    m_Acc_ypr[1]=atan2(Acc.x,sqrt(pow(Acc.y,2.0)+pow(Acc.z,2.0)))*180/M_PI;
    m_Acc_ypr[2]=atan2(Acc.y,sqrt(pow(Acc.x,2.0)+pow(Acc.z,2.0)))*180/M_PI;

    /*m_Gyro_ypr[0]=Gyro.z*((float)*m_Clock)/1000.0+m_Filtered_Data[0];
    m_Gyro_ypr[1]=Gyro.y*((float)*m_Clock)/1000.0+m_Filtered_Data[1];
    m_Gyro_ypr[2]=Gyro.x*((float)*m_Clock)/1000.0+m_Filtered_Data[2];*/
  
     //Update value
    m_Filtered_Data[0]= Gyro.z*((float)*m_Clock)/1000.0;
    m_Filtered_Data[1]= m_Alpha*(-Gyro.y*((float)*m_Clock)/1000.0+m_Filtered_Data[1])+(1-m_Alpha)*m_Acc_ypr[1];
    m_Filtered_Data[2]= m_Alpha*(Gyro.x*((float)*m_Clock)/1000.0+m_Filtered_Data[2])+(1-m_Alpha)*m_Acc_ypr[2];
  
    if(m_p_Filtered!=NULL)
    {
      m_p_Filtered[0]=m_Filtered_Data[0];
      m_p_Filtered[1]=m_Filtered_Data[1];
      m_p_Filtered[2]=m_Filtered_Data[2];
    }

    //m_TestIntegrator= Gyro.y*((float)*m_Clock)/1000.0+m_TestIntegrator;
  
    Disp_Angle();
    Disp_CSV();
    Disp_DEBUG(Gyro);
  }
    
}


float CF1::Get_Angle(int Index)
{
  return m_Filtered_Data[Index];
}

void CF1::Disp_Angle()
{
  if(m_Verbose)
  {
    Serial.print("Yaw:   " +String(m_Filtered_Data[0])+"\t");
    Serial.print("Pitch: " +String(m_Filtered_Data[1])+"\t");
    Serial.println("Roll:  " +String(m_Filtered_Data[2]));
  }
}

void CF1::Disp_CSV()
{
  //extern float Pitch;
  if(m_CSV) // TIME;[FILTERED];[GYRO];[ACC]
  {
    Serial.println(String(millis())+";"+String(m_Filtered_Data[0])+";"+String(m_Filtered_Data[1])+";"+String(m_Filtered_Data[2])+";"+String(m_Gyro_ypr[0])+";"+String(m_Gyro_ypr[1])+";"+String(m_Gyro_ypr[2])+";"+String(m_Acc_ypr[0])+";"+String(m_Acc_ypr[1])+";"+String(m_Acc_ypr[2]));

  }
}

void CF1::Disp_DEBUG(sensors_vec_t Gyro)
{
  if(m_Debug)
  {
    //Serial.println(String(millis())+";"+String(Gyro.x)+";"+String(Gyro.y)+";"+String(Gyro.z));
    Serial.println(String(millis())+";"+String(m_Filtered_Data[1])+";"+String(m_Acc_ypr[1])+";"+String(-Gyro.y));
  }
}

void CF1::Set_Verbose(bool Verbose)
{
  m_Verbose=Verbose;
  if(Verbose && m_CSV)
  {
    m_CSV=false;
  }
  
}

void CF1::Set_CSV(bool CSV)
{
  m_CSV=CSV;
  if(CSV && m_Verbose)
  {
    m_Verbose=false;
  }
}
