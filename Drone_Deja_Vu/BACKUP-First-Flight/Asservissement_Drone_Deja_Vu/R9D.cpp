#include "R9D.h"


R9D::R9D(SBUS *sbus)
{
    m_Left_Joystick_Horizontal=0;
    m_Left_Joystick_Vertical=0; //Throttle
    m_Right_Joystick_Horizontal=0;
    m_Right_Joystick_Vertical=0;
    m_SwitchA=0;
    m_SwitchB=0;
    m_SwitchC=0;
    m_VRC=0; // (Left POTAR)
    m_VRB=0; // (RIGHT POTAR)

    m_sbus=sbus;
}

void R9D::Update()
{
  m_sbus->process();
  m_Left_Joystick_Horizontal=(m_sbus->getChannel(4))/8; //The 8 divider is requiered
  m_Left_Joystick_Vertical=(m_sbus->getChannel(3))/8; //Throttle
  m_Right_Joystick_Horizontal=(m_sbus->getChannel(1))/8;
  m_Right_Joystick_Vertical=(m_sbus->getChannel(2))/8;
  m_SwitchA=(m_sbus->getChannel(9))/8;
  m_SwitchB=0; //TODO: Check the chan of Switch B ^^"
  m_SwitchC=(m_sbus->getChannel(5))/8;
  m_VRC=(m_sbus->getChannel(7))/8; // (Left POTAR)
  m_VRB=(m_sbus->getChannel(8))/8; // (RIGHT POTAR)
}

void R9D::Set_Commands(int *Commands)
{
  Commands[0]=m_Left_Joystick_Horizontal;
  Commands[1]=m_Left_Joystick_Vertical; //Throttle
  Commands[2]=m_Right_Joystick_Horizontal;
  Commands[3]=m_Right_Joystick_Vertical;
  Commands[4]=m_SwitchA;
  Commands[5]=m_SwitchB;
  Commands[6]=m_SwitchC;
  Commands[7]=m_VRC; // (Left POTAR)
  Commands[8]=m_VRB; // (RIGHT POTAR)
}

int R9D::Get_Left_Joystick_Horizontal()
{
  return m_Left_Joystick_Horizontal;
}

int R9D::Get_Left_Joystick_Vertical()
{
  return m_Left_Joystick_Vertical;
}

int R9D::Get_Right_Joystick_Horizontal()
{
  return m_Right_Joystick_Horizontal;
}

int R9D::Get_Right_Joystick_Vertical()
{
  return m_Right_Joystick_Vertical;
}

int R9D::Get_SwitchA()
{
  return m_SwitchA;
}

int R9D::Get_SwitchB()
{
  return m_SwitchB;
}
int R9D::Get_SwitchC()
{
  return m_SwitchC;
}

int R9D::Get_VRC()
{
  return m_VRC;
}

int R9D::Get_VRB()
{
  return m_VRB;
}
